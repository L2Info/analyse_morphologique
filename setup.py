#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='analyse_morphologique',
      version='2.1',
      description='Un logiciel d analyse des verbes conjugués.',
      long_description=readme(),
      classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: Common Public License',
        'Programming Language :: Python :: 3.4',
        'Topic :: Text Processing :: Linguistic',
      ],
      keywords='verbe conjugaison grammaire',
      url='https://gitlab.com/L2Info/analyse_morphologique.git',
      author='Sibylle Louvel, Boris Merminod, Anthony Grau',
      author_email='11296038@foad.iedparis8.net',
      license='CC BY-NC 3.0 FR',
      packages=find_packages(),
      install_requires=[
          'Sphinx',
      ],
      test_suite='nose.collector',
      tests_require=['nose'],
      entry_points = {
          'gui_scripts': [
              'anal-mor = analyse.interfaceQt:launcher',
          ],
      },
      include_package_data=True,
      zip_safe=False)
