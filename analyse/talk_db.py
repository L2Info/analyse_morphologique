#!/usr/bin/python3
# -*- coding: utf-8 -*-

import hashlib
import sys
import os
from operator import itemgetter
from PyQt5.QtSql import *

try:
	from index import fiToList, listToIndex
except ImportError :
	from analyse.index import fiToList, listToIndex

pathAM = ""

def indexToDb(index,nom_table,db, profondeur = 0) :
	"""
	indexToDb : Sauvegarde l'index passé en paramètre dans une table de la base de donnée. Chaque noeud de l'index est une ligne de la 	table. Chaque colonne représente la lettre associée au noeud et la profondeur dans l'index 
	
	Args:
		index (list): Contient l'index arborescent sous la forme de listes imbriquées. Cet index sera parcouru et chacun des éléments sera entré dans une table de la base de donnée
		nom_table (str): Contient le nom de la table qui va contenir l'index dans la base de donnée
		db (QSqlQuery): Contient un objet permettant d'intéragir avec la base de donnée
		profondeur (int): Contient une valeur représentant la profondeur dans l'index arborescent index. Cette profondeur est spécifiée dans la base de donnée
	"""
	if index == [] :
		return
	if isinstance(index[0], list) :
		indexToDb(index[0], nom_table, db,profondeur + 1)
	else :
		if (index[0].isalpha() and len(index[0]) == 1) or index[0] == "-" or index[0] == "#" :
			chaine = "INSERT INTO "+str(nom_table)+" VALUES(\'"+str(index[0])+"\',"+str(profondeur)+");"
		else :
			taille = len(index) - 1
			index = convert(index)
			strtolist = ",".join(index)
			chaine = "INSERT INTO "+str(nom_table)+" VALUES(\'"+str(strtolist)+"\',"+str(profondeur)+");" 
			index = index[taille:]
		db.exec_(chaine)
	indexToDb(index[1:],nom_table, db, profondeur)

def convert(liste) :
	for i in range(len(liste)) :
		liste[i] = str(liste[i])
	return liste


def dbToIndex(rows, index = [], asc=False, profondeur = 0) : 
	"""
	dbToIndex : Récupère un index sauvé dans une table de la base de donnée et le transforme en index utilisable par le programme. Le 	contenu de la base de donnée est récupéré dans une liste rows passée en paramètre à partir de laquelle on va reconstruire index
	
	Args:
		rows (list): Liste dont chaque élément est une liste représentant une ligne d'une table de la base de donnée sous la forme [lettre/entrée , profondeur]
		index (list): Liste qui va contenir l'index reconstruit à partir de la base de donnée
		asc (bool): Permet de régler le parcours de la fonction pour reconstruire fidèlement l'index depuis la base de donnée
		profondeur (int): Permet de connaître la profondeur de l'index arborescent, ce qui aide également à la reconstruction de l'index
	
	Returns:
		(tuple), retour d'un tuple contenant l'index reconstruit, la liste rows vide et le paramètre asc (valant False ou True)
	"""
	if rows == [] :
		return index, [], True
	if rows[0][1] < profondeur :
		return index, rows, True
	if rows[0][1] > profondeur :
		temp, rows, asc = dbToIndex(rows, [], False, profondeur + 1)
		index.append(temp)
		return dbToIndex(rows, index, asc, profondeur)
	if (rows[0][1] == profondeur) and isinstance(rows[0][0],str):
		if asc == True :
			return index, rows, False
		else :
			if len(rows[0][0]) != 1 :
				return rows[0][0].split(","), rows[1:], True
			else :
				index.append(rows[0][0])
	return dbToIndex(rows[1:], index, False, profondeur)
			

def initIndex(listeArg) :
	"""
	initIndex : Effectue à partir de la base de donnée les tâches suivantes :
	- Vérifie les checksums et les dates de modifications des fichiers dictionnaire par rapport au valeurs stockée dans la base de donnée
	- Si checksums et ctime sont inchangé alors la fonction appelle dbToIndex et reconstruit les index à partir des tables sauvées auparavant
	- En cas de changement la fonction ouvre les fichiers dictionnaire reconstruit les index et modifie les tables de la base de donnée pour les mettre à jour
	- Les index sont retournés dans une liste retour 
	
	Args:
		listeArg (list): liste d'arguments dont chaque élément est une liste de la forme ['nomFichier', 'nomTable', '/chemin/nomFichier'], elle contient donc le nom du fichier, le nom d'une table de la base de donnée et le chemin d'accès au fichier
	
	Returns: 
		retour (list), est une liste qui contient l'ensemble des index construit ou reconstruit pendant cette phase
		
	"""
	check = dbToList("MaJ")
	if check == [] :
		return []
	dico_check = {}
	dico_check[check[0][0]] = [check[0][1], check[0][2]]
	dico_check[check[1][0]] = [check[1][1], check[1][2]]
	dico_check[check[2][0]] = [check[2][1], check[2][2]]
	dico_check[check[3][0]] = [check[3][1], check[3][2]]
	retour = []
	for elt in listeArg :
		checksum, ctime = fileCheck(elt[2])
		if checksum == None and ctime == None :
			erreur("initIndex : Echec de lecture d'un fichier")
		if checksum == dico_check[elt[0]][0] and ctime == dico_check[elt[0]][1] :
			print("Reconstruction de l'index {} à partir de la base de donnée".format(elt[1]))
			rows = dbToList(elt[1])
			index = dbToIndex(rows, [], False,0)[0]
		else :
			query = QSqlQuery()
			chaine = "DROP TABLE IF EXISTS "+ elt[1]
			query.exec_(chaine)
			chaine = "UPDATE MaJ SET Checksum=\'"+str(checksum)+"\', Ctime="+str(ctime)+" WHERE Fichier=\'"+str(elt[0])+"\'"
			query.exec_(chaine)
			chaine = "CREATE TABLE "+elt[1]+"(Lettre VARCHAR(100), Profondeur INT)"
			query.exec_(chaine)
			print("Lecture du fichier {}".format(elt[0]))
			liste = fiToList(elt[2])
			print("Reconstruction de l'index {}".format(elt[1]))
			index = listToIndex(liste)
			print("Insertion de l'index dans la base de donnée")
			indexToDb(index,elt[1], query,0)
		retour.append(index)
	return retour
			

def fileCheck(fichier) :
	"""
	fileCheck : Vérification du checksum et du ctime d'un fichier passé en paramètre
	
	Args:
		fichier (str): Contient le nom d'un fichier dont il faut vérifier le checksum et le ctime
	
	Returns:
		(tuple), Retourne un tuple qui contient le checksum (str) et le ctime (int)
	"""
	try :
		with open(fichier, 'rb') as mon_fichier :
			contenu = mon_fichier.read()
		checksum = hashlib.md5(contenu).hexdigest()
		(mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(fichier)
		return checksum, ctime
	except IOError :	
		sys.stderr.write("fileCheck : Erreur contrôler le fichier {}\n".format(fichier))
	except OSError :
		sys.stderr.write("fileCheck : Erreur contrôler le fichier {}\n".format(fichier))
	except :
		sys.stderr.write("fileCheck : Autre erreur\n")
	return None, None	
	

def startDb() :
	"""
	startDb : Démarre le serveur permettant de se connecter à postgresql
	
	Returns:
		retour (int), 0 si tout ce passe bien et >0 sinon
	"""
	retour = 0
	print("\nDémarrage de postgresql\n")
	retour += os.system("sudo -u postgres /usr/local/pgsql/bin/pg_ctl status -D /usr/local/pgsql/data")
	if retour == 0 :
		return retour
	retour = 0
	retour += os.system("sudo -u postgres /usr/local/pgsql/bin/pg_ctl start -D /usr/local/pgsql/data")
	retour += os.system("sleep 5")
	return retour

def stopDb() :
	"""
	stopDb : stoppe le serveur une fois que l'utilisation de postgresql est terminée
	
	Returns:
		retour (int), 0 si tout ce passe bien et >0 sinon
	"""
	print("\nArrêt de postgresql\n")
	retour = os.system("sudo -u postgres /usr/local/pgsql/bin/pg_ctl stop -D /usr/local/pgsql/data")
	retour += os.system("sleep 5")
	return retour

def db_connec(dbUser, dbName) :
	"""
	db_connec : Permet d'établir une connexion avec la base de donnée postgresql
	
	Args:
		dbUser (str): Contient le nom d'utilisateur utilisé pour se connecter à la base de donnée en principe userAM
		dbName (str): Contient le nom de la base de donnée utilisée pour le programme en principe userdb
	
	Returns:
		connec (QSqlDatabase): Retourne un objet qui va permettre la connexion avec la base de donnée
	"""
	if not isinstance(dbUser, str) or not isinstance(dbName, str) :
		erreur("db_connec : Nom de base de donnée ou d'utilisateur non conforme")
	connec = None
	connec = QSqlDatabase.addDatabase("QPSQL")
	connec.setDatabaseName(dbName)
	connec.setUserName(dbUser)
	connec.setHostName("localhost")
	return connec
	

def erreur(arg) :
	"""
	erreur : Fonction appelée pour les erreurs 
	"""
	exit(arg)

def dbToList(tableName) :
	"""
	dbToList : récupère le contenu d'une table de la base de donnée et retourne se contenu sous la forme d'une liste 
	
	Args:
		tableName (str): Contient le nom d'une table de la base qui va permettre de récupérer le contenu de la table
	
	Returns:
		liste (list), Retourne toutes les lignes de la base de donnée sous la forme d'une liste. Chaque élément est une liste de la forme [lettre/entrée, profondeur] 
	"""
	if not isinstance(tableName, str) :
		return []
	model = QSqlTableModel()
	model.setTable(tableName)
	if model.select() == False :
		return []
	nb_ligne = model.rowCount()
	liste = []
	for i in range(nb_ligne) :
		ligne = []
		record = model.record(i)
		nomsColonne = columnName(record)
		for elt in nomsColonne :
			ligne.append(record.value(elt))
		liste.append(ligne)
	return liste

def columnName(record) :
	"""
	columnName : détermine le nom des colonnes qui constitue une table de la base de donnée
	"""
	if not isinstance(record, QSqlRecord) :
		return []
	i = 0
	retour = []
	while 1 :
		chaine = record.fieldName(i)
		if chaine == '':
			break
		retour.append(chaine)
		i+=1
	return retour	

def rawOrRam(index, first) :
	"""
	rawOrRam : Fonction qui accroche la communication avec la base de donnée avec le reste du programme. A partir du résultat de la fonction fileCheck cette fonction va déclencher la récupération des index dans la base de donnée, ou conserver les index reconstruit lors d'opérations précédente
	
	Args:
		index (list): Contient l'index sous la forme de listes imbriquées
		first (bool): Contient un booléen indiquant s'il s'agit du premier appel de la fonction donc de la première connexion
	
	Returns:
		index (list), contient l'index retourné sous la forme de listes imbriquées
	"""
	global pathAM
	pathAM = checkPath()
	if first == True :
		connec = db_connec("userAM", "userdb")
	else :
		connec = QSqlDatabase.database()
	if connec.open() == False :
		erreur("rawOrRam : Echec de connexion")
	
	recon = False	
	check = dbToList("MaJ")
	dico_check = {}
	dico_check[check[0][0]] = [check[0][1], check[0][2]]
	dico_check[check[1][0]] = [check[1][1], check[1][2]]
	dico_check[check[2][0]] = [check[2][1], check[2][2]]
	dico_check[check[3][0]] = [check[3][1], check[3][2]]
	
	for fichier in ["tiroirBrut", "lemmeBrut"] :
		checksum, ctime = fileCheck(pathAM+fichier)
		if (checksum != dico_check[fichier][0]) or (ctime != dico_check[fichier][1]) :
			recon = True
			break
	if recon == True or first == True:
		index = initIndex([["tiroirBrut", "Index_tiroir", pathAM+"tiroirBrut"], ["lemmeBrut", "Index_lemme", pathAM+"lemmeBrut"]])
	connec.close()
	return index		

def checkPath() :
	"""
	checkPath : Cette fonction va rechercher le fichier lemmeBrut dans quelques répertoires clés du système. Si ces répertoire sont introuvable le programme ne pourra pas fonctionner

	Returns:
		chemin d'accès (str), qui sera associé à la variable pathAM pour trouver les fichiers qui feront tourner le programme
	"""
	global pathAM
	try :
		stream = open("/usr/local/lib/python3.4/dist-packages/analyse_morphologique-2.1-py3.4.egg/analyse/lemmeBrut", "r")
		stream.close()
		return "/usr/local/lib/python3.4/dist-packages/analyse_morphologique-2.1-py3.4.egg/analyse/"
	except FileNotFoundError : 
		try : 
			print("checkPath : Impossible de trouver lemmeBrut, je vais chercher dans le répertoire home de l'utilisateur")
			stream = open("~/AM_conf/analyse_morphologique/analyse/lemmeBrut", "r")
			stream.close()
			return "~/AM_conf/analyse_morphologique/analyse/"
		except FileNotFoundError:	
			try :
				print("checkPath : Impossible de trouver lemmeBrut, je vais chercher dans le répertoire courant")
				stream = open("/analyse/lemmeBrut", "r")
				stream.close()
				return "analyse/"
			except FileNotFoundError :
				exit("checkPath : Le fichier lemmeBrut est introuvable je ne peux pas travailler sans")	
	

def main_db():
	global pathAM
	pathAM = checkPath()
	if startDb() != 0 :
		erreur("main_db : échec de démarrage de la base de donnée")
	connec = db_connec("userAM", "userdb")
	if connec.open() == False :
		erreur("main_db : Echec de connexion")
	retour = initIndex([["tiroirBrut", "Index_tiroir", pathAM + "tiroirBrut"], ["lemmeBrut", "Index_lemme", pathAM + "lemmeBrut"]])
	print(retour)
	connec.close()
	
if __name__ == '__main__': 
	if len(sys.argv) == 2 :
		if sys.argv[1] == "-t" :
			stopDb()
	else :
		main_db()
