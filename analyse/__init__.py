#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__version__ = "2.1"

from .index import *
from .interfaceQt import *
from .setup_db import *
from .talk_db import *
