#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from operator import itemgetter

"""Globale: dictionnaire des modèles.[[temps, [flexion, radical, [personne,...]],...]...]"""
modeles = {
    'aimer': ['#', ['indpres', ['1', '1', ['1', '2', '3', '6']], ['1', '2', ['4']], ['2', '2', ['5']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '1', ['1', '2', '3', '4', '5', '6']]]],
    'finir': ['#', ['indpres', ['3', '1', ['1', '2', '3', '4', '5', '6']]], ['impar', ['2', '1', ['1', '2', '3', '4', '5', '6']]], ['simple', ['2', '1', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['2', '1', ['1', '2', '3', '4', '5', '6']]]],
    'battre': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2', '3']], ['1', '2', ['4']], ['2', '2', ['5', '6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['2', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '2', ['1', '2', '3', '4', '5', '6']]]],
    'céder': ['#', ['indpres', ['2', '1', ['1', '2', '3']], ['1', '2', ['4']], ['2', '2', ['5', '6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '3', ['1', '2', '3', '4', '5', '6']]]],
    'broyer': ['#', ['indpres', ['2', '1', ['1', '2', '3', '6']], ['1', '2', ['4']], ['2', '2', ['5']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '3', ['1', '2', '3', '4', '5', '6']]]],
    'assiéger': ['#', ['indpres', ['2', '1', ['1', '2', '3']], ['1', '2', ['4', '5', '6']]], ['impar', ['1', '2', ['1', '2', '3', '6']], ['1', '3', ['4', '5']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '2', ['1', '2', '3', '4', '5', '6']]]],
    'manger': ['#', ['indpres', ['1', '1', ['1', '2', '3', '4', '5', '6']]], ['impar', ['1', '1', ['1', '2', '3', '6']], ['1', '2', ['4', '5']]], ['simple', ['1', '1', ['1', '2', '3', '4', '5']], ['1', '2', ['6']]], ['indfut', ['1', '1', ['1', '2', '3', '4', '5', '6']]]],
    'absoudre': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2']], ['5', '1', ['3']], ['1', '2', ['4']], ['2', '2', ['5', '6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '3', ['1', '2', '3', '4', '5', '6']]]],
    'tenir': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2']], ['5', '1', ['3']], ['1', '2', ['4']], ['2', '2', ['5']], ['6', '1', ['6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['3', '3', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '4', ['1', '2', '3', '4', '5', '6']]]],
    'traire': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2']], ['5', '1', ['3']], ['1', '2', ['4']], ['2', '2', ['5']], ['2', '1', ['6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '1', ['1', '2', '3', '4', '5', '6']]]],
    'courir': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2']], ['5', '1', ['3']], ['1', '1', ['4']], ['2', '1', ['5', '6']]], ['impar', ['1', '1', ['1', '2', '3', '4', '5', '6']]], ['simple', ['4', '1', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '1', ['1', '2', '3', '4', '5', '6']]]],
    'croire': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2']], ['5', '1', ['3']], ['1', '2', ['4']], ['2', '2', ['5']], ['2', '1', ['6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['4', '3', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '1', ['1', '2', '3', '4', '5', '6']]]],
    'croître': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2']], ['5', '1', ['3']], ['1', '2', ['4']], ['2', '2', ['5', '6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['5', '3', ['1', '2', '3', '6']], ['4', '3', ['4', '5']]], ['indfut', ['1', '4', ['1', '2', '3', '4', '5', '6']]]],
    'cueillir': ['#', ['indpres', ['2', '1', ['1', '2', '3', '5', '6']], ['1', '1', ['4']]], ['impar', ['1', '1', ['1', '2', '3', '4', '5', '6']]], ['simple', ['2', '1', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '2', ['1', '2', '3', '4', '5', '6']]]],
    'apprécier': ['#', ['indpres', ['2', '1', ['1', '2', '3', '5', '6']], ['1', '1', ['4']]], ['impar', ['1', '1', ['1', '2', '3', '4', '5', '6']]], ['simple', ['1', '1', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '2', ['1', '2', '3', '4', '5', '6']]]],
    'modeler': ['#', ['indpres', ['2', '1', ['1', '2', '3', '6']], ['1', '2', ['4']], ['2', '2', ['5']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '3', ['1', '2', '3', '4', '5', '6']]]],
    'peser': ['#', ['indpres', ['2', '1', ['1', '2', '3', '6']], ['1', '2', ['4']], ['2', '2', ['5']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '3', ['1', '2', '3', '4', '5', '6']]]],
    'acquérir': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2']], ['5', '1', ['3']], ['1', '2', ['4']], ['2', '2', ['5']], ['2', '3', ['6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['2', '4', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '5', ['1', '2', '3', '4', '5', '6']]]],
    'placer': ['#', ['indpres', ['1', '1', ['1', '2', '3', '5', '6']], ['1', '2', ['4']]], ['impar', ['1', '2', ['1', '2', '3', '6']], ['1', '3', ['4', '5']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5']], ['1', '3', ['6']]], ['indfut', ['1', '1', ['1', '2', '3', '4', '5', '6']]]],
    'joindre': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2']], ['5', '1', ['3']], ['1', '2', ['4']], ['2', '2', ['5', '6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['2', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '3', ['1', '2', '3', '4', '5', '6']]]],
    'mettre': ['#', ['indpres', ['4', '1', ['1']], ['1', '1', ['2', '3']], ['1', '2', ['4']], ['2', '2', ['5', '6']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['2', '3', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '2', ['1', '2', '3', '4', '5', '6']]]],
    'jeter': ['#', ['indpres', ['1', '1', ['1', '2', '3', '6']], ['1', '2', ['4']], ['2', '2', ['5']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '1', ['1', '2', '3', '4', '5', '6']]]],
    'créer': ['#', ['indpres', ['1', '1', ['1', '2', '3', '6']], ['1', '2', ['4']], ['2', '2', ['4']]], ['impar', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['simple', ['1', '2', ['1', '2', '3', '4', '5', '6']]], ['indfut', ['1', '1', ['1', '2', '3', '4', '5', '6']]]]}
correspondances = []
forme = ''
firstCo = True
liste_index = []

def assoc(x, alist):
    """Cette fonction renvoie la sous-liste commençant par l'élément x si elle existe.

Args:
    x (string): l'élément cherché
    alist (list): une liste d'association

Returns:
    sousListe (list), la sous liste associée à x ou None s'il n'y a pas de correspondance.
    """
    for sousListe in alist[1:]:
        if sousListe[0] == x:
            return sousListe
    return None

def fiToList(fichier):
    """
Construit une liste ordonnée à partir du fichier des teminaisons de verbes au format [[terminaison, [temps, flexion, personne]], ...]. Terminaison voit ses lettres inversées.

Construit une liste ordonnée à partir du fichier des lemmes au format [[radical, [lemme, modele, numero]], ...]. Radical voit ses lettres inversées.

Args:
    fichier (string): le nom du fichier à traiter

Returns:
    liste (list), la liste contruite à partir du fichier.
    """
    liste = []
    temp = []
    try:
        flux = open(fichier, "r")
    except IOError:
        message = "Le fichier " + fichier + " n'a pu être ouvert"
        exit(message)
    for line in flux:
        temp = line.split()
        if len(temp) > 1:
            liste += [[temp[0], [x for x in temp[1:]]]]
    flux.close()
    for x in range(len(liste)):
        liste[x][0] = liste[x][0][::-1]
    liste = sorted(liste, key=itemgetter(0))
    return liste

def insere (listeMot, specifications, index):
    """Cette fonction ajoute une terminaison ou un radical et ses spécifications aux index arborescents correspondants.

Args:
    listeMot (list): une liste de lettres formant un mot
    specifications (list): liste [temps, flexion, personne] ou [lemme, modele, numero]
    index (list): l'index arborescent initialisé à ['#'] (voir la fonction listToIndex)

Returns:
    Pas de retour, modifie index.
    """
    branche = []
    lettres = []
    for x in range (1, len(index)):
        lettres += [index [x][0]]
    if not listeMot:
        if len(index) > 1:
            for spec in index[1:]:
                if spec == specifications:
                    break
                else:
                    index += [specifications]
        else:
            index += [specifications]
    elif listeMot[0] in lettres:
        branche = index[lettres.index(listeMot[0]) + 1]
        insere(listeMot[1:], specifications, branche)
    else:
        index += [[listeMot[0]]]
        insere(listeMot, specifications, index)


def listToIndex(liste):
    """Cette fonction construit les index arborescents des terminaisons et des radicaux avec leurs spécifications.

Args:
    liste (list): une liste telle que fournie par la fonction fiToList au format [[terminaison, [temps, flexion, personne]], ...] ou [[radical, [lemme, modele, numero]], ...]

Returns:
    index (list): l'index arborescent construit.
    """
    index = ['#']
    for element in liste:
        insere(list(element[0]), element[1], index)
    return index

def indexToList(index, liste = [], mot = ''):
    """ Cette fonction part d'un index arborescent et recontruit la liste telle qu'en sortie de fiToList.

Args:
    index (list): un index arborescent
    mot (string): le mot formé par les lettres de l'index

Returns:
    liste (list),  au format [[terminaison, [temps, flexion, personne]], ...] ou [[radical, [lemme, modele, numero]], ...]
    """
    for x in index[1:]:
        if len(x) == 3 and isinstance(x[2], str) and isinstance(eval(x[2]), int):
            temp = [[mot, x]]
            if temp[0] not in liste:
                liste += temp
        else:
            for z in index[1:]:
                if len(z[0]) == 1:
                    break
            if x == z:
                mot = mot + x[0]
            else:
                mot = mot[:-1] + x[0]
            indexToList(x, liste, mot)
    return liste

def getTiroir(terminaisons):
    """Cette fonction renvoie les spécifications (tiroir) une fois l'index parcouru. Renvoie une liste vide si le parcours n'est pas complet.

Args:
    terminaisons (list): l'index arborescent des tiroirs.

Returns:
    temp (list), les spécifications (tiroir) correspondant à l'index parcouru ou la liste vide s'il n'y a pas de résultat.
    """
    temp = []
    for x in terminaisons[1:]:
        if len(x[0]) > 1:
            temp += [x]
    else:
        return temp

def getLemme(forme, radicaux):
    """La fonction parcourt l'index et en extrait les spécifications (lemme).

Args:
    forme (list): un radical (l'ordre des lettres doit être inversé).
    radicaux (list): l'index arborescent des radicaux.

Returns:
    radicaux[1:] (list), les spécifications (lemme) correspondant à l'index parcouru ou None si rien ne correspond.
    """
    while forme:
        radicaux = assoc(forme[0], radicaux)
        if not radicaux:
            break
        forme = forme[1:]
    else:
        return radicaux[1:]

def compatible(tiroir, lemme):
    """La fonction récupère les spécifications d'un modèle (variable globale: modeles) donné selon le temps utilisé.
    Elle compare:
    temps des spécifications et du tiroir
    radical des spécifications et du lemme
    vérifie que la personne indiquée dans le tiroir est contenue dans les spécifications

Args:
    tiroir (list): [temps, flexion, personne]
    lemme (list): [lemme, modèle, radical]

Returns:
    1 (VRAI) si toutes les conditions sont valides, 0 (FAUX) sinon.
    """
    specModele = assoc(tiroir[0], modeles[lemme[1]])[1:]
    for elem in specModele:
        if elem[0] == tiroir[1] and elem[1] == lemme[2] and tiroir[2] in elem[2]:
            return 1
    else:
        return 0

def afficheAnalyse(tiroir, lemme):
    """la fonction retourne une chaine de texte décrivant les résultats de la recherche.

Args:
    tiroir: [temps, flexion, personne]
    lemme: [[lemme, modèle, radical]]

Returns:
    temps (string), la chaine de texte correspondant aux spécifications.
    """
    temps = ''
    mot = ''
    position = ''
    personne = ''
    if tiroir[0] == 'indpres':
        temps = "indicatif présent"
    elif tiroir[0] == 'impar':
        temps = "imparfait"
    elif tiroir[0] == 'simple':
        temps = "passé simple"
    elif tiroir[0] == 'indfut':
        temps = "indicatif futur"
    if tiroir[2] in ['1', '2', '3']:
        personne = tiroir[2]
        position = 'du singulier'
    else:
        personne = str(int(tiroir[2]) - 3)
        position = 'du pluriel'
    mot = forme
    temps = mot + ' : lemme ' + lemme[0] + ' (verbe), tiroir ' + temps + ', position ' + personne + 'e personne ' + position
    return temps

def analyse(temp, terminaisons, radicaux):
    """La fonction récupère le tiroir pour une terminaison donnée et le lemme du radical restant.
    Si les variables ne sont pas vides, la fonction vérifie leur compatibilité (fonction compatible) puis ajoute le résultat à la globale correspondances s'il existe (fonction afficheAnalyse).
    La fonction récurse en ajoutant une lettre à la terminaison et en supprimant une lettre du radical. Cela permet de traiter toutes les combinaisons possibles pour un mot donné.

Args:
    temp (list): un mot inversé sous forme de liste
    terminaisons (list): l'index des tiroirs ou l'une de ses branches
    radicaux (list): l'index des lemmes

Returns:
    Pas de retour, modifie correspondances.
    """
    global correspondances
    terminaisons = assoc(temp[0], terminaisons)
    if terminaisons:
        tiroir = getTiroir(terminaisons)
    lemme = getLemme(temp[1:], radicaux)
    if terminaisons and tiroir and lemme:
        for x in tiroir:
            for y in lemme:
                if len(y[0]) > 1 and compatible(x, y):
                    temp = afficheAnalyse(x, y)
                    if temp not in correspondances:
                        correspondances += [afficheAnalyse(x, y)]
    if terminaisons:
        analyse(temp[1:], terminaisons, radicaux)

def anal_res(mot):
    """La fonction met le mot au bon format, construit les index arborescents si besoin et recueille les réponses pour construire une chaîne solution.

Args:
    mot (string): une chaîne contenant le mot à analyser

Returns:
    resultat, la chaîne réponse.
    """
    try:
        from talk_db import rawOrRam
    except ImportError :
        from analyse.talk_db import rawOrRam
    global correspondances
    global forme
    global liste_index
    global firstCo
    correspondances = []
    resultat = ''
    forme = mot
    mot = mot + '-'
    
    #Base de donnée : rajout pour communiquer avec la base de donnée
    if firstCo :
    	liste_index = rawOrRam(liste_index, True)
    	firstCo = False
    else :
    	liste_index = rawOrRam(liste_index, False)
    terminaisons, radicaux = liste_index
    temp = mot[:]
    temp = list(temp[::-1])
    analyse(temp, terminaisons, radicaux)
    for x in correspondances:
        resultat += x
        resultat += '\n'
    return resultat

if __name__ == '__main__':
    correspondances = []
    resultat = ''
    if len(sys.argv) == 2:
        mot = sys.argv[1]
        forme = mot
        mot = mot + '-'
        terminaisons = listToIndex(fiToList('tiroirBrut'))
        radicaux = listToIndex(fiToList('lemmeBrut'))
        temp = mot[:]
        temp = list(temp[::-1])
        analyse(temp, terminaisons, radicaux)
        for x in correspondances:
            resultat += x
            resultat += '\n'
        print (resultat)
    else:
        exit("usage: un argument, le verbe conjugué")
