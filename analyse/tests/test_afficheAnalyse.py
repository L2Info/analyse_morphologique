#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
import analyse.index
 
class TestFonctionAfficheAnalyse(unittest.TestCase):
 
    def setUp(self):
        self.tiroirSing = ['impar', '1', '3']
        self.tiroirPlur = ['impar', '1', '5']
        self.lemme = ['abonner', 'aimer', '1']
 
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")

    def test_afficheAnalyse_sing(self):
        analyse.index.forme = 'abonnait'
        result = analyse.index.afficheAnalyse(self.tiroirSing, self.lemme)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'abonnait : lemme abonner (verbe), tiroir imparfait, position 3e personne du singulier')

    def test_afficheAnalyse_plur(self):
        analyse.index.forme = 'abonniez'
        result = analyse.index.afficheAnalyse(self.tiroirPlur, self.lemme)
        self.assertIsInstance(result, str)
        self.assertEqual(result, 'abonniez : lemme abonner (verbe), tiroir imparfait, position 2e personne du pluriel')

if __name__ == '__main__':
    unittest.main()
