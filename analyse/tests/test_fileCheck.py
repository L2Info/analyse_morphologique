#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import os
from operator import itemgetter
from analyse.index import *
from analyse.talk_db import startDb, stopDb, fileCheck, db_connec, dbToList, checkPath, pathAM
from PyQt5.QtSql import QSqlQuery

class TestFileCheck(unittest.TestCase) :
	def setUp(self):
		global pathAM
		pathAM = checkPath()
		self.dbUser = "userAM"
		self.dbName = "userdb"
		self.file_1 = pathAM+"tests/fileCheck.test"
		self.file_2 = pathAM+"tests/fileCheck_2.test"
		self.connec = None
		
	def tearDown(self):
		os.system("rm -rf __pycache__")
		os.system("rm -rf analyse/__pycache__")
		if self.connec :
			self.connec.close()
	
	def test_fileCheck(self) :
		"""
		test de la fonction fileCheck :
		- Test de la fonction sur un fichier qui ne change jamais (fileCheck.test)
		- Test de la fonction sur un fichier qui change à chaque test (fileChack_2.test)
		"""
		startDb()
		self.connec = db_connec(self.dbUser, self.dbName)
		self.connec.open()
		rows = dbToList("MaJ_test")
		with open(self.file_2,"a") as mon_fichier:
			mon_fichier.write("\nBonjour moi aussi je suis un fichier de test")
		#Comparaison avec fileCheck.test
		checksum, ctime = fileCheck(self.file_1)
		self.assertEqual(checksum, rows[0][1])	
		self.assertEqual(ctime, rows[0][2])
		
		#Comparaison avec fileCheck_2.test
		checksum, ctime = fileCheck(self.file_2)
		self.assertNotEqual(checksum, rows[1][1])
		self.assertNotEqual(ctime, rows[1][2])
		query = QSqlQuery()
		query.exec_("UPDATE MaJ SET Checksum={}, Ctime={} WHERE Fichier={}".format(checksum, ctime, "fileCheck_2.test"))

if __name__ == '__main__':
	unittest.main()
