#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
from analyse.index import fiToList
from analyse.talk_db import checkPath, pathAM
 
class TestFonctionFiToList(unittest.TestCase):

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")

    def test_fiToList_lemme(self):
        pathAM = checkPath()
        liste = fiToList(pathAM+'/tests/lemmeTest')
        self.assertEqual(liste, [['egar', ['rager', 'rager', '1']], ['egnam', ['manger', 'rager', '1']], ['elleppa', ['appeler', 'appeler', '1']]])

    def test_fiToList_tiroir(self):
        pathAM = checkPath()
        liste = fiToList(pathAM+'/tests/tiroirTest')
        self.assertEqual(liste, [['-', ['indprés', '1', '1']], ['-', ['indprés', '1', '3']], ['-s', ['indprés', '1', '2']], ['-sno', ['indprés', '1', '4']], ['-tn', ['indprés', '1', '6']], ['-z', ['indprés', '1', '5']]])

    def test_fiToList_fichier_vide(self):
        pathAM = checkPath()
        liste = fiToList(pathAM+'/tests/fichierVide')
        self.assertEqual(liste, [])

    def test_fiToList_mauvais_fichier(self):
        with self.assertRaises(SystemExit) as cm:
            fiToList('toto')
        self.assertEqual(cm.exception.code, "Le fichier toto n'a pu être ouvert")

if __name__ == '__main__':
    unittest.main()
