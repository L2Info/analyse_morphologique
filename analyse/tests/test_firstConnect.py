#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import os
from operator import itemgetter
from analyse.index import *
from analyse.talk_db import * 
from analyse.setup_db import firstConnect, setupDb, pathAM, checkPath

class TestFirstConnect(unittest.TestCase) :

	def setUp(self):
		self.dbUserTest = "userTest"
		self.dbNameTest = "userDbTest"
		self.connec = None

	def tearDown(self):
		os.system("rm -rf __pycache__")
		os.system("rm -rf analyse/__pycache__")
		if self.connec :
			self.connec.close()
		print("\nSuppression de la base de donnée {}\n".format(self.dbNameTest))
		os.system("sudo -u postgres /usr/local/pgsql/bin/dropdb "+self.dbNameTest)
		print("\nSuppression de l'utilisateur {}\n".format(self.dbUserTest))
		os.system("sudo -u postgres /usr/local/pgsql/bin/dropuser "+self.dbUserTest)
		
	def test_firstConnect(self) :
		"""
		test de la mise en place des tables de la base de donnée :
		- Le test va récupérer le contenu des tables construites par firstConnect et recréer un index à partir de ce contenu
		- Ensuite le test va reconstruire un index directement depuis les fichiers de test.
		- On va comparer les deux types d'index pour vérifier que les tables construites par firstConnect sont justes
		"""
		global pathAM
		pathAM = checkPath()
		startDb()
		setupDb(self.dbUserTest, self.dbNameTest)
		self.connec = db_connec(self.dbUserTest, self.dbNameTest)
		self.connec.open()
		firstConnect([["tiroirTest_2", "Index_tiroir_test", pathAM+"tests/tiroirTest_2"], ["lemmeTest_2", "Index_lemme_test", pathAM+"tests/lemmeTest_2"]], pathAM+"tests/fileCheck.test", pathAM+"tests/fileCheck_2.test")
		check = dbToList("MaJ")
		dico_check = {}
		dico_check[check[0][0]] = [check[0][1] , check[0][2]]
		dico_check[check[1][0]] = [check[1][1] , check[1][2]]	
		for elt in [["tiroirTest_2", "Index_tiroir_test"], ["lemmeTest_2", "Index_lemme_test"]] :
			liste = fiToList(pathAM+"tests/"+elt[0])
			index = listToIndex(liste)
			rows = dbToList(elt[1])
			index_check = dbToIndex(rows, [],0)[0]
			self.assertEqual(index, index_check)
			checksum, ctime = fileCheck(pathAM+"/tests/"+elt[0])
			self.assertEqual(checksum, dico_check[elt[0]][0])
			self.assertEqual(ctime, dico_check[elt[0]][1])

if __name__ == '__main__':
	unittest.main()
	
