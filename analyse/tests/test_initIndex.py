#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
from operator import itemgetter
from analyse.index import * 
from analyse.talk_db import *

class TestDbToIndex(unittest.TestCase) :
	def setUp(self):
		self.dbUser = "userAM"
		self.dbName = "userdb"
		self.tiroir = ['#',['-',['indprés', '1', '1'], ['indprés', '1', '3'],['s', ['indprés', '1', '2'], ['n', ['o', ['indprés', '1', '4']]]], ['t', ['n', ['indprés', '1', '6']]],['z', ['indprés', '1', '5']]]]
		self.lemme = ['#',['e',['g',['a', ['r', ['rager', 'rager', '1']]],['n', ['a', ['m', ['manger', 'rager', '1']]]]], ['l', ['l', ['e', ['p', ['p', ['a', ['appeler', 'appeler', '1']]]]]]]]]
		self.fileTiroir = "tiroirTest_2"
		self.fileLemme = "lemmeTest_2"
		self.connec = None

	def tearDown(self):
		os.system("rm -rf __pycache__")
		os.system("rm -rf analyse/__pycache__")
		with open(pathAM+"tests/tiroirTest", "r") as mon_fichier :
			contenu_tiroir = mon_fichier.read()
		with open(pathAM+"tests/lemmeTest", "r") as mon_fichier :
			contenu_lemme = mon_fichier.read()
		with open(pathAM+"tests/"+self.fileTiroir, "w") as mon_fichier :
			mon_fichier.write(contenu_tiroir)
		with open(pathAM+"tests/"+self.fileLemme, "w") as mon_fichier :
			mon_fichier.write(contenu_lemme)
		print("\nFERMETURE")
		initIndex([[self.fileTiroir, "Index_tiroir_test", pathAM+"tests/"+self.fileTiroir], [self.fileLemme, "Index_lemme_test", pathAM+"tests/"+self.fileLemme]])
		if self.connec :
			self.connec.close()

	def test_consIndex_initIndex(self) :
		"""
		Test de la fonction initIndex lorsqu'un fichier dictionnaire test a subi une modification
		"""
		global pathAM
		pathAM = checkPath()
		startDb()
		self.connec = db_connec(self.dbUser, self.dbName)
		self.connec.open()
		with open(pathAM+"tests/"+self.fileTiroir, "a") as mon_fichier :
			mon_fichier.write("test-\tindtest 1\t6")	
		with open(pathAM+"tests/"+self.fileLemme, "a") as mon_fichier :
			mon_fichier.write("teste\ttester\ttester\t1")

		retour = initIndex([[self.fileTiroir, "Index_tiroir_test", pathAM+"tests/"+self.fileTiroir], [self.fileLemme, "Index_lemme_test", pathAM+"tests/"+self.fileLemme]])	
		self.assertNotEqual(self.tiroir, retour[0])
		self.assertNotEqual(self.lemme, retour[1])
		
		
	
	def test_reconDb_initIndex(self) :
		"""
		test de la fonction initIndex lorsqu'aucun fichier dictionnaire test n'a subi de modification
		"""
		global pathAM
		pathAM = checkPath()
		startDb()
		self.connec = db_connec(self.dbUser, self.dbName)
		self.connec.open() 
		retour = initIndex([[self.fileTiroir, "Index_tiroir_test", pathAM+"tests/"+self.fileTiroir], [self.fileLemme, "Index_lemme_test", pathAM+"tests/"+self.fileLemme]])	
		self.assertEqual(self.tiroir, retour[0])
		self.assertEqual(self.lemme, retour[1])

if __name__ == '__main__':
	unittest.main()
