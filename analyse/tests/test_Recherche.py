#! /usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import unittest
from PyQt5.QtTest import QTest
from PyQt5.QtCore import (QCoreApplication, Qt)

from analyse.interfaceQt import *
from analyse.talk_db import startDb, stopDb


app = QApplication(sys.argv)


class TestRecherche(unittest.TestCase):


    def setUp(self):
    
        self.form = Recherche()
        self.form.show()


    def test_defaults(self):

        self.assertEqual(self.form.instr.text(), "Taper votre recherche puis presser la touche Entrée :")
        self.assertEqual(self.form.inl.text(), "")
        self.assertEqual(self.form.outw.text(), "")
        self.assertEqual(self.form.qbtn.text(), "Quitter")


    def test_EnterKey(self):

        startDb()
        QTest.keyClicks(self.form.inl, "aime")
        self.assertEqual(self.form.inl.text(), "aime")
        QTest.keyClick(self.form.inl, Qt.Key_Return)
        self.assertNotEqual(self.form.outw.text(), "")
        self.assertEqual(self.form.inl.text(), "")
        #stopDb()





if __name__ == '__main__' :

    unittest.main()








