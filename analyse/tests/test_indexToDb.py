#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import os
from operator import itemgetter
from analyse.index import *
from analyse.talk_db import startDb, stopDb, indexToDb, db_connec, dbToList, pathAM, checkPath

class TestIndexToDb(unittest.TestCase) :
	def setUp(self):
		self.dbUser = "userAM"
		self.dbName = "userdb"
		self.tiroir = [['#', 0], ['-', 1], ['indprés,1,1', 2], ['indprés,1,3', 2], ['s', 2], ['indprés,1,2', 3], ['n', 3], ['o', 4], ['indprés,1,4', 5], ['t', 2], ['n', 3], ['indprés,1,6', 4], ['z', 2], ['indprés,1,5', 3]]
		self.lemme = [['#', 0], ['e', 1], ['g', 2], ['a', 3], ['r', 4], ['rager,rager,1', 5], ['n', 3], ['a', 4], ['m', 5], ['manger,rager,1', 6], ['l', 2], ['l', 3], ['e', 4], ['p', 5], ['p', 6], ['a', 7], ['appeler,appeler,1', 8]]
		self.connec = None



	def tearDown(self):
		os.system("rm -rf __pycache__")
		os.system("rm -rf analyse/__pycache__")
		if self.connec :
			self.connec.close()
	
	def test_indexToDb(self) :
		"""
		test de la fonction indexToDb :
		- On construit un index avec fiToList et listToIndex
		- L'index est stocké dans la base de donnée à l'aide de indexToDb
		- On récupère le contenu de indexToDb qui doit correspondre aux attributs tiroir et lemme
		"""
		global pathAM
		pathAM = checkPath()
		startDb()
		self.connec = db_connec(self.dbUser, self.dbName)
		self.connec.open()
		for elt in [["tiroirTest_2", "Index_tiroir_test", self.tiroir], ["lemmeTest_2", "Index_lemme_test", self.lemme]] :
			liste = fiToList(pathAM+"tests/"+elt[0])
			index = listToIndex(liste)
			rows = dbToList(elt[1])
			self.assertEqual(rows, elt[2])
		

if __name__ == '__main__':
	unittest.main()
