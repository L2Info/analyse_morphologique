#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
from analyse.index import assoc, insere, fiToList, listToIndex, getLemme
from analyse.talk_db import checkPath, pathAM
 
class TestFonctionGetLemme(unittest.TestCase):
 
    def setUp(self):
        pathAM = checkPath()
        self.listeRadicaux = fiToList(pathAM+'/tests/lemmeBrut')
        self.radicaux = listToIndex(fiToList(pathAM+'/tests/lemmeBrut'))
 
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")

    def test_getLemme_mot(self):
        for x in self.listeRadicaux:
            lemme = getLemme(x[0], self.radicaux)
            self.assertIn(x[1], lemme)

    def test_getLemme_none(self):
        lemme = getLemme(['o', 't', 'o', 't'], self.radicaux)
        self.assertEqual(lemme, None)

if __name__ == '__main__':
    unittest.main()
