#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
from analyse.index import insere, fiToList, listToIndex, indexToList
from analyse.talk_db import checkPath, pathAM
 
class TestFonctionListToIndex(unittest.TestCase):
 
    def setUp(self):
        pathAM = checkPath()
        self.lemmeTest = fiToList(pathAM+'/tests/lemmeTest')
        self.tiroirTest = fiToList(pathAM+'/tests/tiroirTest')
        self.lemmeBrut = fiToList(pathAM+'/tests/lemmeBrut')
        self.tiroirBrut = fiToList(pathAM+'/tests/tiroirBrut')
 
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")

    def test_listToIndex_lemmeTest(self):
        result = listToIndex(self.lemmeTest)
        reListe = indexToList(result)
        self.assertEqual(result, \
['#', \
    ['e', \
        ['g', \
            ['a', \
                ['r', \
                    ['rager', 'rager', '1']]],
            ['n', \
                ['a', \
                    ['m', \
                        ['manger', 'rager', '1']]]]], \
        ['l', \
            ['l', \
                ['e', \
                    ['p', \
                        ['p', \
                            ['a', \
                                ['appeler', 'appeler', '1']]]]]]]]])
        self.assertEqual(self.lemmeTest, reListe)

    def test_listToIndex_tiroirTest(self):
        result = listToIndex(self.tiroirTest)
        reListe = indexToList(result, [])
        self.assertEqual(result, \
['#', \
    ['-', \
        ['indprés', '1', '1'], \
        ['indprés', '1', '3'], \
        ['s', \
            ['indprés', '1', '2'], \
            ['n', \
                ['o', \
                    ['indprés', '1', '4']]]], \
        ['t', \
            ['n', \
                ['indprés', '1', '6']]], \
        ['z', \
            ['indprés', '1', '5']]]])
        self.assertEqual(self.tiroirTest, reListe)

    def test_listToIndex_lemmeBrut(self):
        result = listToIndex(self.lemmeBrut)
        reListe = indexToList(result, [])
        self.assertEqual(self.lemmeBrut, reListe)

    def test_listToIndex_tiroirBrut(self):
        result = listToIndex(self.tiroirBrut)
        reListe = indexToList(result, [])
        self.assertEqual(self.tiroirBrut, reListe)

if __name__ == '__main__':
    unittest.main()
