#! /usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import unittest
from PyQt5.QtTest import QTest
from PyQt5.QtCore import (QCoreApplication, Qt)

from analyse.interfaceQt import *



app = QApplication(sys.argv)


class TestMaJ(unittest.TestCase):

    def setUp(self):
        
        self.form = MaJ()
        self.form.show()


    def test_defaults(self):

        self.assertEqual(self.form.filebtn.text(), "Choix du fichier")
        self.assertEqual(self.form.instr.text(), "Fichier en cours de modification : ")
        self.assertEqual(self.form.filename.text(), "")
        self.assertEqual(self.form.contents.toPlainText(), "")
        self.assertEqual(self.form.instr2.text(), "")
        self.assertEqual(self.form.savebtn.text(), "Enregistrer")
        self.assertEqual(self.form.cancelbtn.text(), "Annuler")


    def setFormToZero(self):

        self.form.fname = ""
        self.form.filename.setText("")
        self.form.contents.setText("")
        self.form.instr2.setText("")



    def test_affichages(self):
        self.setFormToZero()
        self.form.filename.setText("test d'affichage du nom du fichier")
        self.assertEqual(self.form.filename.text(), "test d'affichage du nom du fichier")
        self.form.contents.setText("test d'affichage dans la fenêtre d'édition de fichiers")
        self.assertEqual(self.form.contents.toPlainText(), "test d'affichage dans la fenêtre d'édition de fichiers")
        self.form.instr2.setText("test d'affichage du msg informatif de sauvegarde")
        self.assertEqual(self.form.instr2.text(), "test d'affichage du msg informatif de sauvegarde")
        



    def test_cancelbtn(self):
        """Test du bouton d'annulation"""
        self.setFormToZero()
        self.form.fname = "test du bouton d'annulation"
        self.form.filename.setText("test du bouton d'annulation")
        self.form.contents.setText("test du bouton d'annulation")
        QTest.mouseClick(self.form.cancelbtn, Qt.LeftButton)
        self.assertEqual(self.form.fname, "")
        self.assertEqual(self.form.filename.text(), "")
        self.assertEqual(self.form.contents.toPlainText(), "")
        self.assertEqual(self.form.instr2.text(), "")
    

    


if __name__ == '__main__' :

    unittest.main()
