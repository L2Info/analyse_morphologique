#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
from analyse.index import assoc
 
class TestFonctionAssoc(unittest.TestCase):
 
    def setUp(self):
        self.alist = ['#', ['a', ['r']], ['b', ['o']]]
        self.anotherlist = ['#', ['i', ['i']]]
 
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")
 
    def test_assoc_element(self):
 
        element = assoc('a', self.alist)
        self.assertEqual(element, ['a', ['r']])

    def test_assoc_redondant(self):
        element = assoc('i', self.anotherlist)
        self.assertEqual(element, ['i', ['i']])
 
    def test_assoc_autre_element(self):
 
        element = assoc('b', self.alist)
        self.assertEqual(element, ['b', ['o']])

    def test_assoc_sous_liste(self):
 
        element = assoc('r', self.alist[1])
        self.assertEqual(element, ['r'])

    def test_assoc_autre_sous_liste(self):
 
        element = assoc('o', self.alist[2])
        self.assertEqual(element, ['o'])

    def test_assoc_redondant_sous_liste(self):
        element = assoc('i', self.anotherlist[1])
        self.assertEqual(element, ['i'])

    def test_assoc_liste_vide(self):
 
        element = assoc('a', [])
        self.assertIsNone(element)

    def test_assoc_element_manquant(self):
 
        element = assoc('z', self.alist)
        self.assertIsNone(element)

if __name__ == '__main__':
    unittest.main()
