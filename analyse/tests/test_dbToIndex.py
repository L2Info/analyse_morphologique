#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import os
from operator import itemgetter
from analyse.index import * 
from analyse.talk_db import startDb, stopDb, indexToDb, dbToIndex, db_connec, dbToList, checkPath, pathAM

class TestDbToIndex(unittest.TestCase) :
	def setUp(self):
		self.dbUser = "userAM"
		self.dbName = "userdb"
		self.connec = None

	def tearDown(self):
		os.system("rm -rf __pycache__")
		os.system("rm -rf analyse/__pycache__")
		if self.connec :
			self.connec.close()
	
	def test_dbToIndex(self) :
		"""
		test de la fonction dbToIndex :
		- Reconstruit un index à partir de la base de donnée
		- Construit un second index à partir de dictionnaires de fichier test
		- Compare à chaque fois l'index reconstruit et l'index initial pour vérifier leur conformité
		"""
		global pathAM
		pathAM = checkPath()
		startDb()
		self.connec = None
		self.connec = db_connec(self.dbUser, self.dbName)
		self.connec.open()
		for elt in [["lemmeTest_2", "Index_lemme_test"],["tiroirTest_2", "Index_tiroir_test"]] :
			liste = fiToList(pathAM+"/tests/"+elt[0])
			index = listToIndex(liste)
			rows = dbToList(elt[1])
			index_check = dbToIndex(rows, [], 0)[0]
			self.assertEqual(index, index_check)

if __name__ == '__main__':
	unittest.main()
