#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
from analyse.index import assoc, insere, fiToList, listToIndex, getTiroir
from analyse.talk_db import checkPath, pathAM
 
class TestFonctionGetTiroir(unittest.TestCase):
 
    def setUp(self):
        pathAM = checkPath()
        self.listeTerminaisons = fiToList(pathAM+'/tests/tiroirBrut')
        self.terminaisons = listToIndex(fiToList(pathAM+'/tests/tiroirBrut'))
 
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")

    def test_getTiroir_nested(self):
        for x in self.listeTerminaisons:
            temp = self.terminaisons[:]
            for z in x[0]:
                temp = assoc(z, temp)
            tiroir = getTiroir(temp)
            for i in tiroir:
                self.assertEqual(len(i), 3)
                self.assertIn(i[0], ['indpres', 'impar', 'simple', 'indfut'])
                self.assertIn(i[1], ['1', '2', '3', '4', '5', '6'])
                self.assertIn(i[2], ['1', '2', '3', '4', '5', '6'])

    def test_getTiroir_vide(self):
        temp = self.terminaisons[:]
        tiroir = getTiroir(temp)
        self.assertEqual(tiroir, [])

if __name__ == '__main__':
    unittest.main()
