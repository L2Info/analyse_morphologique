#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
import analyse.index
from analyse.talk_db import checkPath, pathAM

class TestFonctionAnalyse(unittest.TestCase):
 
    def setUp(self):
            pathAM = checkPath()
            self.terminaisons = analyse.index.listToIndex(analyse.index.fiToList(pathAM+'/tests/tiroirBrut'))
            self.radicaux = analyse.index.listToIndex(analyse.index.fiToList(pathAM+'/tests/lemmeBrut'))
            analyse.index.correspondances = []

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")

    def test_analyse_unique(self):
        analyse.index.forme = 'abonnait'
        temp = list("abonnait-"[::-1]) 
        analyse.index.analyse(temp, self.terminaisons, self.radicaux)
        self.assertEqual(analyse.index.correspondances, ['abonnait : lemme abonner (verbe), tiroir imparfait, position 3e personne du singulier'])

    def test_analyse_multiple(self):
        analyse.index.forme = 'abonnais'
        temp = list("abonnais-"[::-1]) 
        analyse.index.analyse(temp, self.terminaisons, self.radicaux)
        self.assertEqual(analyse.index.correspondances, ['abonnais : lemme abonner (verbe), tiroir imparfait, position 1e personne du singulier', 'abonnais : lemme abonner (verbe), tiroir imparfait, position 2e personne du singulier'])

if __name__ == '__main__':
    unittest.main()
