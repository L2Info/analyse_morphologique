#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
from analyse.index import modeles, compatible

class TestFonctionCompatible(unittest.TestCase):
 
    def setUp(self):
        self.tiroir = ['indpres', '1', '1']
        self.lemme = ['abaisser', 'aimer', '1']
        self.tiroirFalse = ['indpres', '2', '2']
 
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")

    def test_compatible_true(self):
        self.assertTrue(compatible(self.tiroir, self.lemme))

    def test_compatible_false(self):
        self.assertFalse(compatible(self.tiroirFalse, self.lemme))

if __name__ == '__main__':
    unittest.main()
