#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
from analyse.index import insere, fiToList
from analyse.talk_db import checkPath, pathAM
 
class TestFonctionInsere(unittest.TestCase):
 
    def setUp(self):
        pathAM = checkPath()
        self.lemme = fiToList(pathAM+'/tests/lemmeTest')
        self.tiroir = fiToList(pathAM+'/tests/tiroirTest')
        self.index = ['#']
 
    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")

    def test_insere_lemme(self):
        insere(list(self.lemme[0][0]), self.lemme[0][1], self.index)
        self.assertEqual(self.index, \
['#', \
	['e', \
		['g', \
			['a', \
				['r', \
					['rager', 'rager', '1']]]]]])

        insere(list(self.lemme[1][0]), self.lemme[1][1], self.index)
        self.assertEqual(self.index, \
['#', \
	['e', \
		['g', \
			['a', \
				['r', \
					['rager', 'rager', '1']]],
			['n', \
				['a', \
					['m', \
						['manger', 'rager', '1']]]]]]])

    def test_insere_tiroir(self):
        insere(list(self.tiroir[0][0]), self.tiroir[0][1], self.index)
        self.assertEqual(self.index, \
['#', \
	['-', \
		['indprés', '1', '1']]])
        insere(list(self.tiroir[1][0]), self.tiroir[1][1], self.index)
        self.assertEqual(self.index, \
['#', \
	['-', \
		['indprés', '1', '1'], \
		['indprés', '1', '3']]])

if __name__ == '__main__':
    unittest.main()
