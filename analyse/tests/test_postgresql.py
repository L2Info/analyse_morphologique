#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
from analyse.talk_db import startDb, stopDb, db_connec
from analyse.setup_db import setupDb

class TestPostgresql(unittest.TestCase):
	
	def setUp(self):
		self.dbUserTest = "userTest"
		self.dbNameTest = "userDbTest"
		self.dbUser = "userAM"
		self.dbName = "userdb"

	def tearDown(self):
		os.system("rm -rf __pycache__")
		os.system("rm -rf analyse/__pycache__")
		print("\nSuppression de la base de donnée {}\n".format(self.dbNameTest))
		os.system("sudo -u postgres /usr/local/pgsql/bin/dropdb "+self.dbNameTest)
		print("\nSuppression de l'utilisateur {}\n".format(self.dbUserTest))
		os.system("sudo -u postgres /usr/local/pgsql/bin/dropuser "+self.dbUserTest)

	def test_db_connec(self) :
		"""
		Vérifie après installation qu'il est possible de se connecter à la base de donnée :
		- Avec le username : userAM
		- Avec la base de donnée : userdb
		"""
		startDb()
		connec = db_connec(self.dbUser, self.dbName)
		retour = connec.open()
		if connec :
			connec.close()
		self.assertEqual(retour, True)
	
	def test_setup_db(self) :
		"""
		test :
		- Démarrage de postgresql
		- Création d'un utilisateur et d'une nouvelle base de donnée
		- Suppression de la base de donnée et de l'utilisateur
		- Arrêt de postgresql
		"""
		startDb()
		setupDb(self.dbUserTest, self.dbNameTest)
		connec = None
		connec = db_connec(self.dbUserTest, self.dbNameTest)
		retour = connec.open()
		if connec :
			connec.close()
		self.assertEqual(retour, True)

if __name__ == '__main__':
	unittest.main()
