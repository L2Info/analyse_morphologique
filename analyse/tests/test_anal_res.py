#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import os
 
from analyse.talk_db import startDb, stopDb
import analyse.index
 
class TestFonctionAnalRes(unittest.TestCase):
 
    def setUp(self):
        analyse.index.firstCo = True
        startDb()

    def tearDown(self):
        os.system("rm -rf __pycache__")
        os.system("rm -rf analyse/__pycache__")
        #stopDb()

    def test_anal_res_unique(self):
        mot = "abonnait"
        solution = analyse.index.anal_res(mot)
        self.assertEqual(solution.count("\n"), 1)
        self.assertEqual(solution, "abonnait : lemme abonner (verbe), tiroir imparfait, position 3e personne du singulier\n")

    def test_anal_res_multiple(self):
        motMultiple = "abonnais"
        solution = analyse.index.anal_res(motMultiple)
        self.assertEqual(solution.count("\n"), 2)
        self.assertEqual(solution, "abonnais : lemme abonner (verbe), tiroir imparfait, position 1e personne du singulier\nabonnais : lemme abonner (verbe), tiroir imparfait, position 2e personne du singulier\n")

    def test_anal_res_vide(self):
        motVide = "chat"
        solution = analyse.index.anal_res(motVide)
        self.assertEqual(solution.count("\n"), 0)
        self.assertEqual(solution, '')

if __name__ == '__main__':
    unittest.main()
