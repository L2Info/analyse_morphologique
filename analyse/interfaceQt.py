#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import (QFont, QPalette)
from PyQt5.QtCore import (QCoreApplication, Qt)

try :
    from index import anal_res
except ImportError : 
    from analyse.index import anal_res

try :
    from talk_db import startDb, stopDb, checkPath
except ImportError :
    from analyse.talk_db import startDb, stopDb, checkPath




"""QWidget : widget de gestions des modifications de fichier"""
class MaJ(QWidget) :

    def __init__(self):
        super().__init__()
        
        self.initUI()
        
        
    def initUI(self):  
    
        self.setWindowTitle("Mise à jour des fichiers")

        """QPushButton : bouton lançant l'ouverture du browser de fichiers"""
        self.filebtn = QPushButton("Choix du fichier", self)

        self.filebtn.clicked.connect(self.getfile)
        self.filebtn.move(50, 20)
        
        """string : sauvegarde du nom du fichier"""
        self.fname = ''

        self.instr = QLabel("Fichier en cours de modification : ", self)
        self.instr.move(50, 50)

        """QLabel : affichage du nom du fichier en cours de modification"""
        self.filename = QLabel(self)
        self.filename.move(50, 80)

        """QTextEdit : affichage du contenu du fichier"""
        self.contents = QTextEdit(self)	
        self.contents.setGeometry(50, 110, 500, 300)

        """QLabel : affichage informatif de la réussite de la modification du fichier"""
        self.instr2 = QLabel(self)
        self.i2col = QPalette()
        self.i2col.setColor(QPalette.Foreground, Qt.blue)
        self.instr2.setPalette(self.i2col)
        self.instr2.move(50, 430)

        """QPushButton : bouton lançant la sauvergarde des modifications"""
        self.savebtn = QPushButton("Enregistrer", self)
        self.savebtn.clicked.connect(self.savemaj)
        self.savebtn.move(350, 430)

        """QPushButton : bouton lançant la réinitialisation du widget de mise à jour de fichiers"""
        self.cancelbtn = QPushButton("Annuler", self)
        self.cancelbtn.clicked.connect(self.cancelmaj)
        self.cancelbtn.move(450, 430)



    def getfile(self) :
        """
            Ouvre le browser de fichiers pour choisir celui à modifier.
            Affiche le nom du fichier choisi.
            Ouvre le fichier choisi en lecture.
            Affiche le contenu du fichier choisi dans la fenêtre d'édition de texte.

            Args:
                 self (QWidget) : le widget de modification de fichiers

            Returns:
                 Pas de retour.
        """
        self.fname = QFileDialog.getOpenFileName(self, "Choix du fichier à modifier", "~/AM_conf/analyse/")[0]

        if self.fname != "" :
            self.filename.setText(str(self.fname))
            self.filename.adjustSize()
            self.instr2.clear()
            f = open(self.fname, 'r')
            with f :
                data = f.read()
                self.contents.setText(data)
                f.close()



    def savemaj(self) :
        """
            Cette fonction sauvegarde la mise à jour du fichier modifié via l'interface.
            Affiche une boîte de dialogue demandant confirmation de la modification du fichier.
            Ouvre le fichier précédement choisi en écriture.
            Ecrase le contenu original du fichier.
            Remplace le contenu du fichier par le contenu de la fenêtre d'édition de texte.

            Args: 
                 self (QWidget) : le widget de modification de fichiers

            Returns:
                 Pas de retour. Modifie le fichier.
        """

        """QMessageBox : boîte de dialogue demandant confirmation des modifications"""
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText("Confirmation de l'enregistrement des modifications")
        msg.setWindowTitle("Demande de confirmation")
        msg.setInformativeText("Le fichier sera écrasé et réécrit avec le contenu de la fenêtre d'édition de texte")
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)

        """int : valeur retournée par le bouton selectionné"""
        retval = msg.exec_()

        if retval == 1024 :
            data = self.contents.toPlainText()
            f = open(self.fname, 'w')
            f.write(data)
            f.close()
            
            self.instr2.setText('La mise à jour a été sauvegardée !')
            self.instr2.adjustSize()
            self.filename.clear()
            self.contents.clear()
            self.fname = ""




    def cancelmaj(self) :
        """
            Cette fonction est reliée au bouton Annuler de la fenêtre de mise à jour de fichiers et permet de la réinitialiser.
            Efface le nom du fichier précédement choisi.
            Efface le contenu de la fenêtre d'édition de texte.

            Args:
               self (QWidget) : le widget de modification de fichiers

            Returns:
               Pas de retour. Modifie l'affichage du widget.           
        """
        self.fname = ""
        self.filename.clear()
        self.contents.clear()







"""QWidget : widget de gestion de la recherche"""
class Recherche(QWidget):
    
    def __init__(self):
        super().__init__()
        self.initUI()
        
        
    def initUI(self):      

        self.setWindowTitle('Recherche')


        """QLabel : instructions pour l'utilisateur"""
        self.instr = QLabel('Taper votre recherche puis presser la touche Entrée :', self)
        self.instr.move(50, 50)

        """QLineEdit : ligne d'entrée de la recherche"""
        self.inl = QLineEdit(self)
        self.inl.move(50, 100)
        self.inl.setFixedWidth(450)

        """QLabel : fenêtre d'affichage du résultat de la recherche"""
        self.outw = QLabel(self)
        self.outw.move(50, 150)

        
        self.inl.returnPressed.connect(self.onChanged)

        """QPushButton : bouton de sortie du programme"""
        self.qbtn = QPushButton('Quitter', self)
        self.qbtn.clicked.connect(self.quitprog)
        self.qbtn.resize(self.qbtn.sizeHint())
        self.qbtn.move(400, 430)



       
    def onChanged(self):
        """
         Récupère le texte entré dans l'interface par l'utilisateur pour sa recherche.
         Lance le traitement de l'entrée récupérée.
         Affiche le résultat de la recherche dans l'interface.
         Affiche un message informatif s'il n'y a pas de réusltat.
         Réinitialise la ligne d'entrée.

         Args:
            self (QWidget) : le widget de recherche

         Returns:
            Pas de retour. Modifie l'affichage de l'interface.
        """
        
        """string : contenu de la ligne d'entrée"""
        text = self.inl.text()

        """string : résultat de la recherche"""
        res = anal_res(text)
        
        if res == "" :
            self.outw.setText("Il n'existe aucun résultat pour cette entrée.\nIl est possible de mettre à jour les fichiers.\n")
            self.outw.adjustSize()

        else :
            R = res.split(",")
            R2 = "\n\t".join(R)
            self.outw.setText(R2)
            self.outw.adjustSize()
        
        self.inl.clear()




    def quitprog(self) :
        """
         Arrête la base de données puis le programme
        """
        stopDb()
        QApplication.quit()




"""QMainWindow : widget principal contenant les widget de recherche et de modification de fichier"""
class MainWindow(QMainWindow) :

    def __init__(self) :
        super(QMainWindow, self).__init__(None)
        self.mdi = QMdiArea()
        self.setCentralWidget(self.mdi)
        self.setWindowTitle("Analyseur Morphologique")
        self.mdi.tileSubWindows()
        self.setGeometry(100, 50, 1200, 500)

        """QMdiSubWindow : partie affichant le widget de mise à jour de fichiers"""
        majw = QMdiSubWindow()
        majw.setWidget(MaJ())
        self.mdi.addSubWindow(majw)
        majw.show()

        """QMdiSubWindow : partie affichant le widget de rehcerche"""
        searchw = QMdiSubWindow()
        searchw.setWidget(Recherche())
        self.mdi.addSubWindow(searchw)
        searchw.show()



    def closeEvent(self, event):
        """
            Cette fonction affiche une boîte de dialogue demandant confirmation de l'arrêt du programme en cas d'appel du bouton 'X'

            Args:
               self (QMainWindow) : le widget principal
               event (QCloseEvent) : évènement envoyé par le bouton 'X' quand il est appelé

            Returns:
               Pas de retour.
        """
        reply = QMessageBox.question(self, 'Message', "Voulez-vous quitter le programme ?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes :
            """Base de donnée : Inclusion stop db"""
            stopDb()
            event.accept()
        else :
            event.ignore()



def launcher():
    checkPath()
    startDb()
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())




"""Exécution de l'application"""
if __name__ == '__main__':
    
    """Base de donnée : Inclusion démarrage db"""
    checkPath()
    startDb()
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())
