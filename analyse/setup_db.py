#!/usr/bin/python3
# -*- coding: utf-8 -*-

import hashlib
import sys
import os
from operator import itemgetter
from PyQt5.QtSql import *

try :
	from index import fiToList, listToIndex
except ImportError :
	from analyse.index import fiToList, listToIndex

try :
	from talk_db import indexToDb, fileCheck, startDb, stopDb, dbToList, db_connec, erreur, checkPath , pathAM
except ImportError :
	from analyse.talk_db import indexToDb, fileCheck, startDb, stopDb, dbToList, db_connec, erreur, checkPath, pathAM



def firstConnect(listeArgu, fileCheck_1, fileCheck_2) :
	"""
	firstConnect : permet de construire les tables qui seront utilisées dans la base de donnée
	
	Args:
		listeArgu (list): liste dont chaque élément est une liste de la forme [chemin/nomFichier, nomTable]
		fileCheck_1 (str): chemin d'accès au fichier fileCheck.test
		fileCheck_2 (str): chemin d'accès au fichier fileCheck_2.test
	"""
	query = QSqlQuery()
	query.exec_("DROP TABLE IF EXISTS Index_tiroir;") 
	query.exec_("DROP TABLE IF EXISTS Index_lemme;") 
	query.exec_("DROP TABLE IF EXISTS MaJ;")
	query.exec_("DROP TABLE IF EXISTS MaJ_test;") 
	query.exec_("DROP TABLE IF EXISTS Index_lemme_test;")
	query.exec_("DROP TABLE IF EXISTS Index_tiroir_test;")
	print("\nConstruction de la table Index_tiroir\n")
	query.exec_("CREATE TABLE Index_tiroir(Lettre VARCHAR(100), Profondeur INT)")
	print("\nConstruction de la table Index_lemme\n")
	query.exec_("CREATE TABLE Index_lemme(Lettre VARCHAR(100), Profondeur INT)")
	print("\nConstruction de la table MaJ\n")
	query.exec_("CREATE TABLE MaJ(Fichier VARCHAR(250), Checksum VARCHAR(250), Ctime INT)")
	print("\nConstruction de la table MaJ_test\n")
	query.exec_("CREATE TABLE MaJ_test(Fichier VARCHAR(250), Checksum VARCHAR(250), Ctime INT)")
	print("\nConstruction de la table Index_tiroir_test\n")
	query.exec_("CREATE TABLE Index_tiroir_test(Lettre VARCHAR(100), Profondeur INT)")
	print("\nConstruction de la table Index_lemme_test\n")
	query.exec_("CREATE TABLE Index_lemme_test(Lettre VARCHAR(100), Profondeur INT)")
	for elt in  listeArgu:
		liste = fiToList(elt[2])
		print("\nEntrée de l'index {} dans la table {}\n".format(elt[0], elt[1]))
		index = listToIndex(liste)
		checksum, ctime = fileCheck(elt[2])
		if checksum == None or ctime == None :
			sys.stderr("firstConnect : Erreur vérifier les fichiers dictionnaires\n")
			exit(1)
		print("\nEntrée du checksum et du ctime dans la table MaJ\n")
		chaine = "INSERT INTO MaJ VALUES(\'"+elt[0]+"\',\'"+checksum+"\',"+str(ctime)+")"
		query.exec_(chaine)
		indexToDb(index,elt[1], query, 0)
		checksum, ctime = fileCheck(fileCheck_1)
	print("\nMise à jour de la table MaJ_test\n")
	chaine = "INSERT INTO MaJ_test VALUES(\'fileCheck.test\',\'"+checksum+"\',"+str(ctime)+")"
	query.exec_(chaine)
	checksum, ctime = fileCheck(fileCheck_2)
	chaine = "INSERT INTO MaJ_test VALUES(\'fileCheck_2.test\',\'"+checksum+"\',"+str(ctime)+")"
	query.exec_(chaine)


def setupDb(dbUser, dbName) :
	"""
	setupDb : création de la base de donnée lors du premier lancement du programme
	
	Args:
		dbUser (str): Contient le nom d'utilisateur pour la connexion à la base de donnée en principe userAM
		dbName (str): Contient le nom de la base de donnée utilisée pour le programme en principe userdb
	
	Returns:
		retour (int), Retourne 0 si tout va bien et >0 dans d'autre cas
	"""
	if (not isinstance(dbUser, str)) or (not isinstance(dbName, str)) :
		erreur("setupDb : le nom de la base de donnée ou de l'utilisateur ne sont pas conforme")
	retour = 0
	print("\nCréation de l'utilisateur {} sur postgresql\n".format(dbUser))
	chaine = "sudo -u postgres /usr/local/pgsql/bin/createuser {}".format(dbUser)
	retour += os.system(chaine)
	print("\nCréation de la base de donnée {} pour l'utilisateur {} sur postgresql\n".format(dbName, dbUser))
	chaine = "sudo -u postgres /usr/local/pgsql/bin/createdb {} -O {}".format(dbName, dbUser)
	retour += os.system(chaine)
	return retour
	

def main_setup():
	global pathAM
	pathAM = checkPath()
	if startDb() != 0 :
		erreur("main_setup : Echec de démarrage de la base de donnée")
		
	setupDb("userAM", "userdb")
	connec = db_connec("userAM", "userdb")
	if connec.open() == False :
		erreur("main_setup : Echec de la connexion avec la data base")
	firstConnect([["tiroirBrut", "Index_tiroir", pathAM+"tiroirBrut"], ["lemmeBrut", "Index_lemme", pathAM+"lemmeBrut"],["tiroirTest_2", "Index_tiroir_test", pathAM+"/tests/tiroirTest_2"], ["lemmeTest_2", "Index_lemme_test", pathAM+"/tests/lemmeTest_2"]], pathAM+"/tests/fileCheck.test", pathAM+"/tests/fileCheck_2.test")
	connec.close()

if __name__ == '__main__':
	main_setup()
