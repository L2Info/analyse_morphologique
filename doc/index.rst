.. Analyse morphologique documentation master file, created by
   sphinx-quickstart on Fri Apr 22 08:50:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Analyse morphologique's documentation!
=================================================

Bienvenue !

Analyse morphologique est un logiciel qui analyse les formes verbales des verbes au présent, imparfait, passé simple et futur.

Documentation:

Lancer le fichier doc/_build/html/index.html pour afficher la documentation dans votre navigateur.

Pré-requis:

Python3-setuptools, python3-pip, Qt5, PyQt5, PostgreSQL

Contents:

.. toctree::
   :maxdepth: 2

   utilisateur
   code



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

