Analyse morphologique: utilisateur
==================================

Pré-requis:

sudo apt-get update

sudo apt-get install python3-setuptools

sudo apt-get install python3-pip

Installation:

Usage:

Après installation et depuis la console, lancer la commande:
anal-mor

Saisir une forme verbale dans la zone de saisie de la fenêtre recherche.

Le résultat de l'analyse s'affiche après avoir appuyé sur la touche entrée.
